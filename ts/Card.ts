enum Suit {
    Spades,
    Hearts,
    Diamonds,
    Clubs,
}
class Card {
    public rank : number;
    public suit: number;
    public constructor(rank: number, suit: Suit) {
        this.rank = rank;
        this.suit = suit;
    }

    private static rankNames = [
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        'J',
        'Q',
        'K',
        'A'
    ]
    public get rankName (): string {
        return Card.rankNames[this.rank -1];
     
    }
    public get suitName (): string { 
        return Suit[this.suit];
    } 
    public get name (): string {
        switch(this.suitName) {
            case 'Spades': {
                return this.rankName + "♠";
            }
            case 'Diamonds': {
                return this.rankName + "♦" ;
            }
            case 'Clubs': {
                return this.rankName + "♣" ;
            }
            case 'Hearts': {
                return this.rankName + "♥";
            }
            default: {
                return this.rankName + this.suitName;
            }
        } 
    }
}