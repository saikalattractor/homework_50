class Deck {
    private cards: Card[];

    public constructor(){
        this.cards = [];

        for(let i = 0; i < 4; i++){
            for(let j = 0; i <= 13; j++){
                this.cards.push(new Card(i, j));
            }
        }
    }
    public shuffle (): void {
        this.cards.sort(() => Math.floor(Math.random() * 3 - 1));
    }

    public draw (): Card {
        return <Card> this.cards.shift();
    }
}
var d = new Deck();
d.shuffle();
console.log(d.draw().name);
