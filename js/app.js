var Suit;
(function (Suit) {
    Suit[Suit["Spades"] = 0] = "Spades";
    Suit[Suit["Hearts"] = 1] = "Hearts";
    Suit[Suit["Diamonds"] = 2] = "Diamonds";
    Suit[Suit["Clubs"] = 3] = "Clubs";
})(Suit || (Suit = {}));
var Card = /** @class */ (function () {
    function Card(rank, suit) {
        this.rank = rank;
        this.suit = suit;
    }
    Object.defineProperty(Card.prototype, "rankName", {
        get: function () {
            return Card.rankNames[this.rank - 1];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Card.prototype, "suitName", {
        get: function () {
            return Suit[this.suit];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Card.prototype, "name", {
        get: function () {
            switch (this.suitName) {
                case 'Spades': {
                    return this.rankName + "♠";
                }
                case 'Diamonds': {
                    return this.rankName + "♦";
                }
                case 'Clubs': {
                    return this.rankName + "♣";
                }
                case 'Hearts': {
                    return this.rankName + "♥";
                }
                default: {
                    return this.rankName + this.suitName;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Card.rankNames = [
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        'J',
        'Q',
        'K',
        'A'
    ];
    return Card;
}());
var Deck = /** @class */ (function () {
    function Deck() {
        this.cards = [];
        for (var i = 0; i < 4; i++) {
            for (var j = 0; i <= 13; j++) {
                this.cards.push(new Card(i, j));
            }
        }
    }
    Deck.prototype.shuffle = function () {
        this.cards.sort(function () { return Math.floor(Math.random() * 3 - 1); });
    };
    Deck.prototype.draw = function () {
        return this.cards.shift();
    };
    return Deck;
}());
var d = new Deck();
d.shuffle();
console.log(d.draw().name);
//# sourceMappingURL=app.js.map